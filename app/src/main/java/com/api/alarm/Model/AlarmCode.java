package com.api.alarm.Model;

import com.api.alarm.Helpers.IRandomNumberWrapper;

/**
 * Created by danielzajork on 11/11/14.
 */
public class AlarmCode {

    private IRandomNumberWrapper randomWrapper;
    private String randomCode;

    public AlarmCode(IRandomNumberWrapper randomWrapper) {

        this.randomWrapper = randomWrapper;
        this.randomCode = this.createCode();
    }

    public String createCode() {

        StringBuilder builder = new StringBuilder();

        builder.append(randomWrapper.getInt());
        builder.append(randomWrapper.getInt());
        builder.append(randomWrapper.getInt());
        builder.append(randomWrapper.getInt());

        return builder.toString();
    }

    public String getCode() {

        return this.randomCode;
    }

    public boolean verifyCode(String code) {

        return code.equals(this.getCode());
    }
}
