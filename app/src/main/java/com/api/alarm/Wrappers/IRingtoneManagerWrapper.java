package com.api.alarm.Wrappers;

import android.net.Uri;

/**
 * Created by danielzajork on 11/11/14.
 */
public interface IRingtoneManagerWrapper {
    Uri getAlarmUri();
}
