package com.api.alarm.Wrappers;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import com.api.alarm.Wrappers.IMediaManager;

import java.io.IOException;

public class MediaManager implements IMediaManager {

    private MediaPlayer mediaPlayer;

    public MediaManager() {


    }

    @Override
    public void stopSound() {

        if (mediaPlayer != null) {

            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    @Override
    public void playSound(Context context, Uri alert) {

        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(context, alert);

        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {

            mediaPlayer.prepare();
            mediaPlayer.start();
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
    }
}
