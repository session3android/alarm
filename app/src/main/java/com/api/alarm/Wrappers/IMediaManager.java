package com.api.alarm.Wrappers;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;

/**
 * Created by danielzajork on 11/11/14.
 */
public interface IMediaManager {
    void stopSound();

    void playSound(Context context, Uri alert) throws IOException;
}
