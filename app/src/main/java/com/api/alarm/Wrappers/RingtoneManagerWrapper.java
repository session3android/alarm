package com.api.alarm.Wrappers;

import android.media.RingtoneManager;
import android.net.Uri;

import com.api.alarm.Wrappers.IRingtoneManagerWrapper;

public class RingtoneManagerWrapper implements IRingtoneManagerWrapper {

    @Override
    public Uri getAlarmUri() {

        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        if (alert == null) {

            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            if (alert == null) {

                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }

        return alert;
    }
}
