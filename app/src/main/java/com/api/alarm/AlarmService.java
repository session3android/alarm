package com.api.alarm;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;

import com.api.alarm.Wrappers.IMediaManager;
import com.api.alarm.Wrappers.IRingtoneManagerWrapper;
import com.api.alarm.Wrappers.MediaManager;
import com.api.alarm.Wrappers.RingtoneManagerWrapper;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.util.*;

public class AlarmService extends Service {

    private IMediaManager mediaManager;
    private IRingtoneManagerWrapper ringtoneManager;
    private Camera camera;
    private Parameters params;

    public AlarmService() {

        mediaManager = new MediaManager();
        ringtoneManager = new RingtoneManagerWrapper();
    }

    // setter injection
    public void setMediaManager(IMediaManager manager) {

        this.mediaManager = manager;
    }

    // setter injection
    public void setRingtoneManager(IRingtoneManagerWrapper ringtoneManagerWrapper) {

        this.ringtoneManager = ringtoneManagerWrapper;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

            Uri path = Uri.parse("android.resource://com.api.alarm/" + R.raw.fg);
            Log.i("info", path.toString());
        try {
            mediaManager.playSound(getBaseContext(), path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Timer timer = new Timer();

            if (camera == null) {
                try {
                    camera = Camera.open();
                    params = camera.getParameters();
                } catch (RuntimeException e) {
                    Log.e("Camera Error. Failed to Open. Error: ", e.getMessage());
                }

            }

            FlashCamera flashCamera = new FlashCamera();
            flashCamera.camera = camera;
            flashCamera.params = params;

            timer.schedule(flashCamera, 11500);

         return super.onStartCommand(intent, flags, startId);
    }

    class FlashCamera extends TimerTask {
        public Camera camera;
        public Parameters params;

        public void run() {

            Log.i("info", "torch is turn on!");
            params.setFlashMode(Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            camera.startPreview();        }
    }

   /* private void flashCameraLed() throws IOException {
        if (camera == null) {
            try {
                camera = Camera.open();
                params = camera.getParameters();
            } catch (RuntimeException e) {
                Log.e("Camera Error. Failed to Open. Error: ", e.getMessage());
            }

        }

        Log.i("info", "torch is turn on!");
        params.setFlashMode(Parameters.FLASH_MODE_TORCH);
        camera.setParameters(params);
        camera.startPreview();
    }
*/
    private void flashCameraLedOff() throws IOException {
        Log.i("info", "Flash is turned off!");
        params.setFlashMode(Parameters.FLASH_MODE_OFF);
        camera.setParameters(params);
        camera.stopPreview();
        camera.release();
    }

    @Override
    public void onDestroy() {

        mediaManager.stopSound();
         try {
             this.flashCameraLedOff();
         } catch (IOException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }
}
