package com.api.alarm.Helpers;

import java.util.Random;

/**
 * Created by danielzajork on 11/11/14.
 */
public class RandomNumberWrapper implements IRandomNumberWrapper {

    @Override
    public int getInt() {

        Random random = new Random();
        return random.nextInt(9);
    }
}
