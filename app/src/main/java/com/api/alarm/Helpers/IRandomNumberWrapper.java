package com.api.alarm.Helpers;

/**
 * Created by danielzajork on 11/11/14.
 */
public interface IRandomNumberWrapper {
    int getInt();
}
