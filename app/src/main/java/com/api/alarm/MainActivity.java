package com.api.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.api.alarm.Dialogs.PinDialog;
import com.api.alarm.Helpers.RandomNumberWrapper;
import com.api.alarm.Model.AlarmCode;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends Activity implements PinDialog.NoticeDialogListener {

    Button setAlarmButton;
    Button stopAlarmButton;
    TextView codeTextView;
    PendingIntent pendingIntent;
    AlarmCode alarmCode;

    Boolean isAlarmSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        setAlarmButton = (Button) this.findViewById(R.id.setAlarm);
        setAlarmButton.setOnClickListener(setAlarmOnClickListener);

        stopAlarmButton = (Button) this.findViewById(R.id.stopAlarmButton);
        stopAlarmButton.setOnClickListener(stopAlarmOnClickListener);

        alarmCode = new AlarmCode(new RandomNumberWrapper());

        codeTextView = (TextView) this.findViewById(R.id.codeTextView);
        codeTextView.setText(alarmCode.getCode());

        this.setIsAlarmSet(isAlarmSet);
    }

    public View.OnClickListener setAlarmOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, onTimeListener, 0, 0, false);
            timePickerDialog.show();
        }
    };

    public View.OnClickListener stopAlarmOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           DialogFragment newFragment = new PinDialog();
           newFragment.show(MainActivity.this.getFragmentManager(), "Alarm");
        }
    };

    public TimePickerDialog.OnTimeSetListener onTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            Calendar calNow = Calendar.getInstance();
            Calendar calSet = (Calendar) calNow.clone();

            calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSet.set(Calendar.MINUTE, minute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if(calSet.compareTo(calNow) <= 0) {
                calSet.add(Calendar.DATE, 1);
            }

            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 1, intent, 0);
            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(), pendingIntent);

            MainActivity.this.setIsAlarmSet(true);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setIsAlarmSet(boolean isSet) {

        this.isAlarmSet = isSet;

        if (isSet) {

            this.setAlarmButton.setVisibility(View.GONE);
            this.stopAlarmButton.setVisibility(View.VISIBLE);
            this.codeTextView.setVisibility(View.VISIBLE);
        } else {

            this.setAlarmButton.setVisibility(View.VISIBLE);
            this.stopAlarmButton.setVisibility(View.GONE);
            this.codeTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDialogPositiveClick(String code) {

        if (alarmCode.verifyCode(code)) {

            stopService(new Intent (getApplicationContext(), AlarmService.class));

            this.setIsAlarmSet(false);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // do nothing
    }
}