package com.api.alarm.Helpers;

import com.api.alarm.AlarmReceiverTest;
import com.api.alarm.Model.AlarmCode;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by danielzajork on 11/11/14.
 */
public class AlarmCodeTest extends TestCase {

    AlarmCode sut = null;

    IRandomNumberWrapper randomNumberWrapperMock = null;

    @Override
    protected void setUp() throws Exception {

        super.setUp();

        randomNumberWrapperMock = new RandomNumberWrapperMock();

        sut = new AlarmCode(randomNumberWrapperMock);
    }

    public void test_verifyCode_given_correct_code_expect_true() {

        boolean result = sut.verifyCode("0000");

        Assert.assertTrue(result);
    }

    public void test_verifyCode_given_incorrect_code_expect_false() {

        boolean result = sut.verifyCode("1234");

        Assert.assertFalse(result);
    }

    public void test_getCode_expect_correct_code() {

        String result = sut.getCode();

        Assert.assertEquals(result, "0000");
    }

    public class RandomNumberWrapperMock implements IRandomNumberWrapper {

        @Override
        public int getInt() {
            return 0;
        }
    }
}
