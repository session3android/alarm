package com.api.alarm;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.test.AndroidTestCase;
import android.test.mock.MockContext;

import com.api.alarm.Wrappers.IMediaManager;
import com.api.alarm.Wrappers.IRingtoneManagerWrapper;

import junit.framework.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AlarmServiceTest extends AndroidTestCase {

    private AlarmService sut;

    private RingtoneManagerMock ringtoneManagerMock = null;
    private MediaManagerMock mediaManagerMock = null;


    @Override
    protected void setUp() throws Exception {
        super.setUp();

        sut = new AlarmService();

        ringtoneManagerMock = new RingtoneManagerMock();
        mediaManagerMock = new MediaManagerMock();

        sut.setMediaManager(mediaManagerMock);
        sut.setRingtoneManager(ringtoneManagerMock);
    }

    public void test_onBind_expect_null() {

        Context context = new AlarmServiceMockContext();
        Intent intent = new Intent(context, AlarmReceiver.class);

        IBinder result = sut.onBind(intent);

        Assert.assertNull(result);
    }

    public void test_onDestroy_expect_stopSound() {

        sut.onDestroy();

        Assert.assertTrue(mediaManagerMock.verifyStopSound());
    }

    public void test_onStartCommand_expect_play_sound() {

        Context context = new AlarmServiceMockContext();
        Intent intent = new Intent(context, AlarmReceiver.class);

        mediaManagerMock.setPlaySoundExpectations(context, ringtoneManagerMock.getAlarmUri());

        sut.onStartCommand(intent, 1, 1);

        Assert.assertTrue(mediaManagerMock.verifyPlaySound());
    }

    public class RingtoneManagerMock implements IRingtoneManagerWrapper {

        @Override
        public Uri getAlarmUri() {
            return Uri.parse("http://www.apihealthcare.com");
        }
    }

    public class MediaManagerMock implements IMediaManager {

        boolean stopSoundCalled = false;
        boolean playSoundCalled = false;

        private Context expectedPlaySoundContext = null;
        private Uri expectedPlaySoundUri = null;

        public void setPlaySoundExpectations(Context context, Uri alert) {

            expectedPlaySoundContext = context;
            expectedPlaySoundUri = alert;
        }

        @Override
        public void stopSound() {
            stopSoundCalled = true;
        }

        @Override
        public void playSound(Context context, Uri alert) throws IOException {

            if (expectedPlaySoundUri.equals(this.expectedPlaySoundUri)) {

                playSoundCalled = true;
            }
        }

        public boolean verifyPlaySound() {

            return playSoundCalled;
        }

        public boolean verifyStopSound() {

            return stopSoundCalled;
        }
    }

    public class AlarmServiceMockContext extends MockContext {

        @Override
        public String getPackageName() {
            return "com.api.alarm.test";
        }
    }
}