package com.api.alarm;

import android.content.ComponentName;
import android.content.Intent;
import android.test.AndroidTestCase;
import android.test.mock.MockContentProvider;
import android.test.mock.MockContext;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielzajork on 11/11/14.
 */
public class AlarmReceiverTest extends AndroidTestCase {

    private AlarmReceiver sut;
    private AlarmReceiverMockContext context;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        sut = new AlarmReceiver();
        context = new AlarmReceiverMockContext();
    }

    public void test_onReceive_starts_service() {

        Intent intent = new Intent(context, AlarmReceiver.class);

        sut.onReceive(context, intent);

        List<Intent> serviceIntents = context.getServiceIntents();

        Intent serviceIntent = serviceIntents.get(0);

        Assert.assertEquals("com.api.alarm.AlarmService", serviceIntent.getComponent().getClassName());
    }

    public class AlarmReceiverMockContext extends MockContext {

        private List<Intent> serviceIntents = new ArrayList<Intent>();

        @Override
        public String getPackageName() {
            return "com.api.alarm.test";
        }

        @Override
        public ComponentName startService(Intent intent)
        {
            serviceIntents.add(intent);

            return null;
        }

        public List<Intent> getServiceIntents()
        {
            return serviceIntents;
        }
    }
}
